import { TestBed, inject, async, getTestBed } from '@angular/core/testing';
import {
  HttpModule,
  Http,
  Response,
  ResponseOptions,
  XHRBackend,
  BaseRequestOptions
} from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { ProductService } from './product.service';

let backend: MockBackend;
let service: ProductService;

describe('ProductService', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpModule ],
      providers: [
        BaseRequestOptions,
        MockBackend,
        ProductService,
        {
          deps: [
            MockBackend,
            BaseRequestOptions
          ],
          provide: Http,
          useFactory: (backend: XHRBackend, defaultOptions: BaseRequestOptions) => {
              return new Http(backend, defaultOptions);
          }
        }
      ]
    });
    const testbed = getTestBed();
    service = testbed.get(ProductService);
  }));

  it('should be created', inject([ProductService], (service: ProductService) => {
    expect(service).toBeTruthy();
  }));

  describe('.getProduct()', () => {
    it('should return an Observable<Array<Product>>', () => {
      [
        {
          product_name: 'Macbook',
        },
        {
          product_name: 'Iphone',
        },
        {
          product_name: 'Keyboard',
        }
      ]

      service.getProducts().subscribe((data) => {
          expect(data.length).toBe(3);
          expect(data[0].title).toBe('Macbook');
          expect(data[1].title).toBe('Iphone');
          expect(data[2].title).toBe('Keyboard');
      });
    });
  });
});
