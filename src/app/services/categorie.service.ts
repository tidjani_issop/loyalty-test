import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CategorieService {

  constructor(private http: HttpClient) { }

  getCategories() {
    return this.http.get('https://test-recrutement.loyaltyexpert.net/categories');
  }

}
