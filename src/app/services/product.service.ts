import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from "@angular/common/http";

@Injectable()
export class ProductService {

  constructor(private http: HttpClient) { }

  getAllProducts(limit: string, start?: string, search?: string) {
    let params = new HttpParams()
      .set('limit', limit)
      .set('start', start)
      .set('search', search)

    return this.http.get('https://test-recrutement.loyaltyexpert.net/products',
      { params, observe: 'response' }
    );
  }

  getProduct(id) {
    return this.http.get('https://test-recrutement.loyaltyexpert.net/products/'+ id);
  }

  addProduct(product) {
    return this.http.post('https://test-recrutement.loyaltyexpert.net/products',
      { name: product.name,
        description: product.description,
        categories: product.categories,
        brand: product.brand.id
      });
  }

  updateProduct(productId, product) {
    return this.http.put('https://test-recrutement.loyaltyexpert.net/products/' + productId,
      { name: product.name,
        description: product.description,
        categories: product.categories,
        brand: product.brand.id
      });
  }
}
