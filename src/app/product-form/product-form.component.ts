import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { CategorieService } from '../services/categorie.service';
import { BrandService } from '../services/brand.service';
import { ProductService } from '../services/product.service';

import { values } from 'lodash';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  @Input() submitForm: Function;
  @Input() formValue: FormGroup;

  brands = [];
  categories = [];
  error: string = '';
  updateMode: boolean = false;

  constructor(private categorieService: CategorieService,
              private brandService: BrandService,
              private productService: ProductService,
              private route: ActivatedRoute) {

      this.route.params.subscribe(params => {
        this.updateMode = params['id'] ? true : false;
      });
    }

  ngOnInit() {
    // required field Validators
    this.formValue.setValidators(([Validators.required]));

    // Retrieve categories & brands for the select input
    this.categorieService.getCategories().subscribe(
      data => this.categories = values(data),
      error => {
        console.error(error);
        this.error = error;
      }
    );

    this.brandService.getBrands().subscribe(
      data => this.brands = values(data),
      error => {
        console.error(error);
        this.error = error;
      }
    );
  }
}
