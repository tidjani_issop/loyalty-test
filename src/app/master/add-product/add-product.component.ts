import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ProductService } from '../../services/product.service';

import { map } from 'lodash';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  error: string = '';
  formValue: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private productService: ProductService) {}

  ngOnInit() {
    this.formValue = this.formBuilder.group({
      name: '',
      description: '',
      categories: [],
      brand: [],
    });
  }

  // Callback
  createProduct (product) {
    product.categories = map(product.categories, 'id');
    this.productService.addProduct(product).subscribe(product => {
      this.formValue.reset();
      alert("Product succefully created !");
    });
  }

}
