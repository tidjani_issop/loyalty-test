import { Component, OnInit } from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import { ProductService } from '../../services/product.service';

import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/takeWhile";
import "rxjs/add/operator/startWith";

import { values } from 'lodash';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private productService: ProductService,
    private observableMedia: ObservableMedia) { }

  products = [];
  error: string = '';
  cols: Observable<number>;
  // pagination
  pageSize: number = 12;
  totalCount: string;
  showSearch: boolean = false;

  ngOnInit() {
    // Get all products
    this.retrieveProducts();
    const grid = new Map([
      ["xs", 1],
      ["sm", 2],
      ["md", 2],
      ["lg", 3],
      ["xl", 3]
    ]);

    let start: number;

    grid.forEach((cols, mqAlias) => {
      if (this.observableMedia.isActive(mqAlias)) {
        start = cols;
      }
    });

    this.cols = this.observableMedia.asObservable()
      .map(change => { return grid.get(change.mqAlias) })
      .startWith(start);
  }

  retrieveProducts(pageIndex: number = 0, search: string = '') {
    let start = 0;
    if (pageIndex && pageIndex > 0) {
      start = this.pageSize * pageIndex;
    }
    this.productService.getAllProducts(this.pageSize.toString(), start.toString(), search).subscribe(
      res => {
        this.totalCount = res.headers.get('X-Pagination-Count');
        this.products = values(res.body);
      },
      error => {
        console.error(error);
        this.error = error;
      }
    );
  }

}
