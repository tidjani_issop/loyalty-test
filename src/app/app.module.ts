import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MasterComponent } from './master/master.component';
import { ProductsComponent } from './master/products/products.component';
import { AddProductComponent } from './master/add-product/add-product.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { UpdateProductComponent } from './update-product/update-product.component';

import { ProductService } from './services/product.service';
import { CategorieService } from './services/categorie.service';
import { BrandService } from './services/brand.service';

const appRoutes: Routes = [
  { path: '', component: MasterComponent },
  { path: 'product/:id', component: UpdateProductComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MasterComponent,
    ProductsComponent,
    AddProductComponent,
    ProductFormComponent,
    UpdateProductComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    MaterialModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    ReactiveFormsModule
  ],
  providers: [ProductService, CategorieService, BrandService],
  bootstrap: [AppComponent]
})
export class AppModule { }
