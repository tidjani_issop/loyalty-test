import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ProductService } from '../services/product.service';

import { map } from 'lodash';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {

  error: string = '';
  formValue: FormGroup;
  product: any;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private productService: ProductService) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
       this.productService.getProduct(params['id']).subscribe(data => {
         this.product = data;

         this.formValue = this.formBuilder.group({
           name: this.product.name,
           description: this.product.description,
           categories: [ this.product.categories ],
           brand: this.product.brand,
         });
       });
    });
  }

  updateProduct = (product) => {
    product.categories = map(product.categories, 'id');
    this.productService.updateProduct(this.product.id, product).subscribe(product => {
      this.formValue.reset();
      this.router.navigate(['/']);
    });
  }
}
